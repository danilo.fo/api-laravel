-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           8.0.30 - MySQL Community Server - GPL
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Copiando estrutura para tabela minha_api.blog
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `text` text,
  `image` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Copiando dados para a tabela minha_api.blog: ~7 rows (aproximadamente)
INSERT IGNORE INTO `blog` (`id`, `uri`, `title`, `description`, `text`, `image`, `updated_at`, `created_at`) VALUES
	(1, 'title-name-1', 'title name 1', 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac erat sit amet sem ultricies sagittis vel ac odio. Nulla luctus commodo ex nec accumsan. Maecenas a consectetur felis, a tristique dolor. Integer sit amet arcu non tellus maximus semper. Nulla metus nunc, blandit tempor libero nec, venenatis posuere urna. Maecenas id viverra risus. Pellentesque et lorem id enim condimentum varius. Donec sed elit rutrum, sagittis magna vitae, egestas purus. Proin vehicula, enim ut consectetur rutrum, leo augue porta nisl, ut iaculis sapien dui id lorem. Maecenas semper erat a mauris pellentesque placerat.\r\n</p>\r\n<p>\r\nMorbi pharetra venenatis erat et aliquam. Donec venenatis egestas risus quis bibendum. Nam vitae elementum ligula. Donec vitae sodales nulla, quis ornare lectus. Mauris ut nibh varius, rhoncus lacus at, suscipit risus. Etiam dictum eros eget mauris dictum condimentum. Vivamus ornare venenatis mauris non pretium.\r\n</p>\r\n<p>\r\nNam orci urna, fermentum et enim vel, efficitur auctor neque. Integer volutpat tortor nulla, a feugiat erat malesuada volutpat. In augue lorem, euismod quis dui sed, convallis egestas dolor. Integer sollicitudin tellus ac nibh molestie volutpat. Sed feugiat, neque ac volutpat rhoncus, tellus mi auctor lectus, ac facilisis magna purus aliquam leo. Cras nulla ex, rhoncus ac felis vitae, maximus euismod risus. Vestibulum eget finibus leo, et euismod sem. Proin varius, tellus id aliquam molestie, dui lorem vehicula risus, a finibus urna sapien vitae sem. Quisque elementum vehicula lacus, id vehicula ex sollicitudin ac. Ut sodales, ante ac lobortis feugiat, nisi velit congue tellus, eu auctor magna massa et justo. Maecenas sit amet mattis leo, et tristique nibh. Pellentesque faucibus quam ac dolor interdum, sit amet commodo odio condimentum. Maecenas a leo congue, posuere massa mattis, dapibus tortor.\r\n</p>\r\n<p>\r\nSuspendisse suscipit molestie odio, ut mattis nulla hendrerit ut. Praesent sit amet erat rhoncus, consequat mauris at, consequat nulla. Aliquam erat volutpat. Etiam metus velit, pulvinar vitae feugiat eu, hendrerit id tortor. Praesent tempus viverra ipsum, eget elementum tortor aliquet ac. Proin eleifend sollicitudin rhoncus. Phasellus vel elit eget leo sollicitudin varius. Quisque laoreet sagittis elit sed placerat. Sed at sapien mollis justo maximus mollis. Curabitur accumsan pulvinar fringilla. Praesent viverra sodales elit eget iaculis.\r\n</p>\r\n<p>\r\nSed placerat dolor in efficitur laoreet. Phasellus ultrices commodo ullamcorper. Aenean vel volutpat lorem. Nunc pharetra, mi at fringilla feugiat, tellus elit egestas tortor, eu pulvinar nunc lectus a quam. Nam diam est, posuere in convallis vitae, rutrum id orci. Phasellus efficitur lorem nisi, tristique finibus mauris feugiat id. Aenean ex leo, feugiat at accumsan quis, maximus eu libero. Aenean quis justo feugiat, aliquet nibh eget, eleifend mi. Duis porttitor fringilla diam sed placerat.\r\n</p>', 'example.jpg', '2023-10-14 08:15:41', '2023-10-13 08:15:41'),
	(2, 'title-name-2', 'title name 2', 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.', 'text blog', 'example.jpg', '2023-10-13 08:25:36', '2023-10-13 08:25:36'),
	(3, 'title-name-3', 'title name 3', 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.', 'text blog', 'example.jpg', '2023-10-13 08:25:37', '2023-10-13 08:25:37'),
	(4, 'title-name-4', 'title name 4', 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.', 'text blog', 'example.jpg', '2023-10-13 08:25:38', '2023-10-13 08:25:38'),
	(5, 'title-name-5', 'title name 5', 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.', 'text blog', 'example.jpg', '2023-10-13 08:25:39', '2023-10-13 08:25:39'),
	(6, 'title-name-6', 'title name 6', 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.', 'text blog', 'example.jpg', '2023-10-13 08:25:40', '2023-10-13 08:25:40'),
	(9, 'nome-do-post', 'Nome do post', 'description description description description description description description description description description description description description description description description description description description description', '<h3><strong>Text</strong></h3><p>TExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt text</p><p>TExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt textTExt text</p>', 'cadeira-spazio_8.jpg', '2023-11-03 14:55:10', '2023-11-03 14:55:10');

-- Copiando estrutura para tabela minha_api.upload
CREATE TABLE IF NOT EXISTS `upload` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `file` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Copiando dados para a tabela minha_api.upload: ~2 rows (aproximadamente)
INSERT IGNORE INTO `upload` (`id`, `type`, `file`) VALUES
	(1, 'image', 'banner-desktop.jpg'),
	(2, 'image', 'banner-desktop_1.jpg'),
	(3, 'image', 'banner-desktop_2.jpg'),
	(4, 'image', 'cadeira-spazio.jpg'),
	(5, 'image', 'cadeira-spazio_1.jpg'),
	(6, 'image', 'cadeira-spazio_2.jpg'),
	(7, 'image', 'cadeira-spazio_3.jpg'),
	(8, 'image', 'cadeira-spazio_4.jpg'),
	(9, 'image', 'cadeira-spazio_5.jpg'),
	(10, 'image', 'cadeira-spazio_6.jpg'),
	(11, 'image', 'cadeira-spazio_7.jpg'),
	(12, 'image', 'cadeira-spazio_8.jpg');

-- Copiando estrutura para tabela minha_api.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Copiando dados para a tabela minha_api.users: ~1 rows (aproximadamente)
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `password`, `updated_at`, `created_at`) VALUES
	(6, 'Danilo Fernando', 'eng.danilofernando@gmail.com', '$2y$10$jvNjy7wCj5OeitAWU7LW/uqtHYFjpCVcXS4QhWOAds65.SzXiy2PC', '2023-10-06 12:42:13', '2023-10-06 12:42:13');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
