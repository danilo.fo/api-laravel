<?php

namespace App\Http\Controllers;

use App\Models\blog;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{

    public function getAll(Request $request)
    {


        $qtdPage = $request->qtd ?? 10;
        $paginaAtual = $request->page ?? 1;

        Paginator::currentPageResolver(function () use ($paginaAtual) {
            return $paginaAtual;
        });
        $blog = DB::table('blog');
        if (!empty($request->busca)) {
            $blog = $blog
                ->where('title', 'like', '%' . str_replace(' ', '%', $request->busca) . '%')
                ->orWhere('description', 'like', '%' . str_replace(' ', '%', $request->busca) . '%')
            ;
        }

        $blog = $blog->orderBy('id','DESC')->paginate($qtdPage)->toArray();


        return response()->json($blog, 200);

    }

    public function get(Request $request)
    {

        $post = blog::where(!empty($request->id) ? 'id' : 'uri', $request->id ?? $request->uri)->first();

        if (!$post) {

            return response()->json([
                'status' => 203,
                'error' => true,
                'msg' => 'Não foi possível localizar a postagem.'
            ], 203);
        }

        return response()->json([
            'status' => 200,
            'error' => false,
            'success' => true,
            'data' => $post,
        ], 203);

    }

    public function save(Request $request)
    {

        if (!$request->title || !$request->description || !$request->text || !$request->uri) {
            return response()->json([
                'status' => 400,
                'error' => true,
                'msg' => 'Campos incompletos'
            ], 400);
        }


        $blog = Blog::updateOrCreate(['id' => !empty($request->id) ? $request->id : null], [
            'title' => $request->title,
            'uri' => $request->uri,
            'description' => $request->description,
            'text' => $request->text,
            'image' => $request->image ?? '',
        ]);

        if (!$blog) {
            return response()->json([
                'status' => 200,
                'error' => true,
                'msg' => 'Erro ao salvar post'
            ], 200);
        }

        return response()->json([
            'status' => 200,
            'error' => false,
            'blog' => $blog,
            'msg' => 'post salvo com sucesso!'
        ], 200);

    }

    public function remove(Request $request)
    {


        if (empty($request->id)) {
            return response()->json([
                'status' => 200,
                'error' => true,
                'msg' => 'Id não informado'
            ], 200);
        }


        $remove = blog::where('id', $request->id)->delete();

        if (empty($remove)) {
            return response()->json([
                'status' => 200,
                'error' => true,
                'msg' => 'Post não encontrado'
            ], 200);
        }


        return response()->json([
            'status' => 200,
            'error' => false,
            'msg' => 'Post removido com sucesso!'
        ], 200);


    }
}
