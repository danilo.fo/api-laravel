<?php

namespace App\Http\Controllers;

use App\Models\Upload;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function upload(Request $request)
    {

        $files[] = $request->file;
        $resultado = false;

        foreach ($files as $file) {

            $nome_final = $this->checarExistencia($file->getClientOriginalName(), '', 0, 'image');
            $file->move('public/uploads', $nome_final);
            @chmod('public/uploads/' . $nome_final, 0644);

            if ($nome_final) {
                $resultado[] = $nome_final;
            }
        }

        if (is_array($resultado) && count($resultado) == 1) {
            $resultado = current($resultado);
        }

        if (empty($resultado)) {
            return response()->json([
                'status' => 203,
                'error' => true,
                'msg' => 'Não foi possível efetuar o upload!',
            ], 200);
        }


        return response()->json([
            'status' => 200,
            'error' => false,
            'msg' => 'Upload feito com sucesso!',
            'file' => $resultado
        ], 200);

    }

    public function checarExistencia($name = '', $realName = '', $number = 0, $type = 'Não definido')
    {


        if (!empty($name)) {
            $result = Upload::where('file', $name)->get()->first();
            if (!$result) {
                Upload::create(['file' => $name, 'type' => $type]);
            } else {
                $number++;
                $name = $realName ? $realName : $name;
                $file_name = pathinfo($name, PATHINFO_FILENAME);
                $file_extension = pathinfo($name, PATHINFO_EXTENSION);
                $newName = $file_name . '_' . $number . '.' . $file_extension;
                $name = $this->checarExistencia($newName, $name, $number, $type);
            }

            return $name;
        }
    }
}
