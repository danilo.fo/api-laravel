<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function create(Request $request)
    {

        if (!$request->name || !$request->email || !$request->password) {
            return response()->json([
                'status' => 400,
                'error' => true,
                'msg' => 'Campos incompletos'
            ], 400);
        }


        if (User::where('email', $request->email)->first()) {
            return response()->json([
                'status' => 400,
                'error' => true,
                'msg' => 'E-mail já cadastrado'
            ], 400);

        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        if ($user) {
            return response()->json([
                'status' => 200,
                'error' => false,
                'msg' => 'Usuário criado com sucesso!'
            ], 200);
        }

    }

    public function login(Request $request)
    {

        $data = $request;

        $user = User::where('email', $data->email)->first();

        if (!$user) {

            return response()->json([
                'status' => 203,
                'error' => true,
                'msg' => 'Usuário inexistente'
            ], 203);
        }

        if (!Hash::check($data->password, $user->password)) {

            return response()->json([
                'status' => 203,
                'error' => true,
                'msg' => 'Usuário ou senha incorreta'
            ], 203);

        }


        $request->session()->put('user', $user);


        return response()->json([
            'status' => 200,
            'error' => false,
            'msg' => 'Login Efetuado com sucesso!'
        ], 200);


    }

    public function checkSession()
    {


        if (!session()->has('user')) {
            return response()->json([
                'status' => 200,
                'error' => false,
                'msg' => 'Usuário não está logado',
                'user' => null
            ], 200);
        }


        return response()->json([
            'status' => 200,
            'error' => false,
            'msg' => 'Usuário logado',
            'user' => session()->get('user')
        ], 200);

    }
}
