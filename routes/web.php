<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(LoginController::class)->group(function () {

    Route::post('/create', 'create')->name('login.create');
    Route::match(['get', 'post'], '/login', 'login')->name('login.login');
    Route::get( '/check-session', 'checkSession')->name('login.check');
});


Route::match(['get', 'post'], '/upload', [UploadController::class,'upload'])->name('upload');
Route::match(['get', 'post'], '/blog/save', [BlogController::class,'save'])->name('blog.save');
Route::match(['get', 'post'], '/blog', [BlogController::class,'getAll'])->name('blog.getAll');
Route::match(['get', 'post'], '/blog/{uri}', [BlogController::class,'get'])->name('blog.get');
Route::match(['post'], '/post/get', [BlogController::class,'get'])->name('post.get');
Route::match(['post'], '/post/remove', [BlogController::class,'remove'])->name('post.remove');
